package models;


import annot.UrlFonction;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Dept {
    private int id;
    private String nomDept;
    private Date dateCreation;

    static int x=0;
    private static ArrayList<Dept> depts=new ArrayList<>();

    public Date getDateCreation() {
        return dateCreation;
    }

    public static ArrayList<Dept> getDepts() {
        return depts;
    }

    public static void setDepts(ArrayList<Dept> depts) {
        Dept.depts = depts;
    }
    public static void addDept(Dept d) {
        depts.add(d);
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Dept(int id, String nomDept) {
        this.id = id;
        this.nomDept = nomDept;
    }

    public Dept() {

    }

    public Dept(int id, String nomDept, Date dateCreation)
    {

        this.id = id;
        this.nomDept = nomDept;
        this.dateCreation = dateCreation;
    }

    @UrlFonction(lien = "dept-save.do")
    public ModelView save(){
        depts.add(this);
        return  list();
    }
    @UrlFonction(lien = "dept-list.do")
    public ModelView list(){
        if(x==0){
            depts.add(new Dept(11,"Commercial",new Date(2019,11,16)));
            depts.add(new Dept(24,"RH",new Date(2020,02,8)));
            depts.add(new Dept(43,"Informatique",new Date(2022,05,25)));
            x++;
        }
        String url="/list-dept.jsp";
        HashMap hashMap=new HashMap();
        hashMap.put("Depts",depts);
        return new ModelView(url,hashMap);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }
}