<%--
  Created by IntelliJ IDEA.
  User: Yohann
  Date: 18/11/2022
  Time: 08:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="jakarta.servlet.http.*"%>
<%@ page import="jakarta.servlet.*"%>
<%@ page import="models.Dept"%>
<%@ page import="java.util.ArrayList" %>
<%  ArrayList<Dept> depts=(ArrayList<Dept>) request.getAttribute("Depts"); %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Liste departement</title>

</head>
<body>
    <table border="1">
        <th>Id Departement</th>
        <th>Nom Departement</th>
        <th>Date de creation</th>
        <% for (int i = 0; i < depts.size() ; i++) { %>
        <tr>
            <td><%=depts.get(i).getId()%></td>
            <td><%=depts.get(i).getNomDept()%></td>
            <td><%=depts.get(i).getDateCreation().toGMTString()%></td>
        </tr>
        <%}%>
    </table>
</body>
</html>
